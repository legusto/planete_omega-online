export class Jeu {

    constructor() {

        this.code = Math.random().toString(36).substr(2, 5);
        console.log(this.code);

        // Instanciation du client Socket.IO pour communiquer avec le serveur
        this.socket = io("https://schatel.dectim.ca:3009", {
            query: {type: 'jeu', id: this.code} // identification en tant que jeu
        });

        this.socket.on("joueur1", message => {
            document.getElementById("messages").innerText += "joueur1: " + JSON.stringify(message) + "\n";
        });

        this.socket.on("joueur2", message => {
            document.getElementById("messages").innerText += "joueur2: " + JSON.stringify(message) + "\n";
        });

        this.socket.on('selectionChoix1', this.faireChoix1.bind(this));
        this.socket.on('selectionChoix2', this.faireChoix2.bind(this));
        this.socket.on('selectionChoix3', this.faireChoix3.bind(this));
        this.socket.on('selectionChoix4', this.faireChoix4.bind(this));
        this.socket.on('selectionChoix5', this.faireChoix5.bind(this));
        this.socket.on('selectionChoix6', this.faireChoix6.bind(this));
        this.socket.on('selectionChoix7', this.faireChoix7.bind(this));
        this.socket.on('selectionChoix8', this.faireChoix8.bind(this));
        this.socket.on('selectionChoix9', this.faireChoix9.bind(this));
        this.socket.on('selectionChoix10', this.faireChoix10.bind(this));
        this.socket.on('fermerIntro', this.jeuIntro.bind(this));


        /// * CREATE.JS
        this.canvas = document.querySelector("canvas");
        // this.canvas.width = window.innerWidth;
        // this.canvas.height = window.innerHeight;

        // this.loop1 = null;
        // this.loop2 = null;
        // this.loop3 = null;
        // this.loop4 = null;
        // this.loop5 = null;

        this.pointage = 0;

        /*AJOUTER ECRAN LOADER PCQ CEST LONG EN S'IL VOUS PLAIT AVEC TOUTES LES RESSOURCES MERCI*/

        this
            .precharger('ressources/manifest.json')
            .then(this.initialiserCreateJS.bind(this))
            .then(this.demarrer.bind(this))
            .catch(erreur => console.error(erreur));

    }//FIN CONSTRUCTOR


//// INITIALISATION STRUCTURE CREATE.JS

    precharger(manifeste = "manifest.json") {

        // Renvoi d'une promesse qui s'exécutera à la fin du chargement
        return new Promise((resolve, reject) => {
            this.chargeur = new createjs.LoadQueue();
            this.chargeur.installPlugin(createjs.Sound);
            this.chargeur.addEventListener('complete', resolve);
            this.chargeur.addEventListener('error', reject);
            this.chargeur.loadManifest(manifeste);
        });
    }

    initialiserCreateJS() {
        this.stage = new createjs.Stage(this.canvas);
        createjs.Ticker.on("tick", e => this.updateSize());
        createjs.Ticker.on("tick", e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;
    }

    //UPDATE DES TAILLES EN À CHAQUE TICK CAR ELLE EST APPELER PAR LE TICKER LIGNE 108
    updateSize() {
        this.canvas.width = 1280;
        this.canvas.height = 720;
        // this.canvas.style.margin= '0 auto';

        //// Tableau pour resize les video qui sont dedans
        // this.tableauVideo[0].videoWidth = window.innerWidth;
        // this.tableauVideo[0].videoHeight = window.innerHeight;
        // console.log(this.canvas.height)
    }

    journaliser(texte) {

        // Ajout du texte à la boîte "journal"
        let element = document.getElementById("journal").textContent += texte + "\n";

        // Défilement automatique
        window.scrollTo(0, document.body.scrollHeight);
    }

    demarrer() {
        document.getElementById('chargement').style.display = "none";

        this.qrCode = new createjs.Bitmap(this.chargeur.getResult('qr'));
        this.qrCode.scaleX = 0.333;
        this.qrCode.scaleY = 0.333;
        this.qrCode.x = 680 - this.qrCode.getBounds().width / 5;
        this.qrCode.y = this.canvas.height / 2 + this.qrCode.getBounds().height / 2.5;

        //ÉCRAN TITRE
        this.titleScreen = new createjs.Bitmap(this.chargeur.getResult('title'));

        this.codeGenerated = new createjs.Text('Veuillez entrer le code suivant sur votre mobile : ' + (this.code), '31px Arial', "#ff7700");
        this.codeGenerated.x = this.canvas.width / 2 + 125;
        this.codeGenerated.y = this.canvas.height / 2 + this.canvas.height * 3.85;
        this.codeGenerated.cache(0, 0, this.codeGenerated.getBounds().width, 150);

        this.stage.addChild(this.titleScreen, this.qrCode, this.codeGenerated);
    }


    jeuIntro() {
        this.stage.removeChild(this.qrCode, this.titleScreen);
        createjs.Tween
            .get(this.codeGenerated)
            .to({alpha : 0},3000)

        //CREATION VIDEO ET DEPLOIEMENT SUR STAGE;
        let videoIntro = this.chargeur.getResult('intro');
        videoIntro.loop = false;
        videoIntro.muted = true;
        videoIntro.alpha = 0;

        // console.log(videoIntro);
        let bitmapIntro = new createjs.Bitmap(videoIntro);
        this.stage.addChild(bitmapIntro);
        videoIntro.play();

        // console.log(bitmapIntro);
        bitmapIntro.scaleX = 0.6667;
        bitmapIntro.scaleY = 0.6667;
        // bitmapIntro.alpha = 0;

        ////REPOSITIONEMENT VIDEO INDIVIDUELLEMENT
        // console.log(this.tableauVideo, this.tableauVideo[0].videoHeight, this.tableauVideo[0].videoWidth);
        // this.tableauVideo[0].videoWidth = this.tableauVideo[0].videoWidth/2;

        //Animation de la couleur du background
        this.canvas.style.backgroundColor = "#E7F7FF";

        this.narrIntro = createjs.Sound.play("narr_intro", {volume: .6});

        //AUDIO DE L'INTRO
        this.loop1 = createjs.Sound.play("loop_1", {volume: .1, loop: -1});
        createjs.Tween
            .get(this.loop1)
            .to({volume: 1}, 6000)

        setTimeout(this.ajoutArbres.bind(this), 24000);

    }


    jeuEtape1() {
        /*beau graphique*/
    }


    avionAnim(){
        this.videoAvion = document.createElement('video');
        this.videoAvion.src = 'ressources/video/avion.webm';

        this.bufferAvion = new createjs.VideoBuffer(this.videoAvion);

        this.bitmapAvion = new createjs.Bitmap(this.bufferAvion);
        this.bitmapAvion.scaleX = 0.6667;
        this.bitmapAvion.scaleY = 0.6667;
        this.stage.addChild(this.bitmapAvion);
    }

    avionInterval(){
        this.videoAvion.play();
    }

    fuseeAnim(){
        this.videoFusee = document.createElement('video');
        this.videoFusee.src = 'ressources/video/fusee.webm';

        this.bufferFusee = new createjs.VideoBuffer(this.videoFusee);

        this.bitmapFusee = new createjs.Bitmap(this.bufferFusee);
        this.bitmapFusee.scaleX = 0.6667;
        this.bitmapFusee.scaleY = 0.6667;
        this.stage.addChild(this.bitmapFusee);
    }

    fuseeInterval(){
        this.videoFusee.play();
    }

    ajoutArbres(){
        //CREATION VIDEO ET DEPLOIEMENT SUR STAGE;
        let arbresGood = this.chargeur.getResult('arbresGood');
        arbresGood.loop = false;
        // arbresGood.alpha = 0;

        let bitmapArbresGood = new createjs.Bitmap(arbresGood);
        this.stage.addChildAt(bitmapArbresGood, 2);
        arbresGood.play();

        bitmapArbresGood.scaleX = 0.6667;
        bitmapArbresGood.scaleY = 0.6667;
    }

    //Autobus
    faireChoix1(choix) {

        /* creation video intro 1 */
        this.introVideoChoix1 = document.createElement('video');
        this.introVideoChoix1.src = 'ressources/video/intro-choix1.webm';

        this.bufferIntro1 = new createjs.VideoBuffer(this.introVideoChoix1);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix1 = new createjs.Bitmap(this.bufferIntro1);
        this.bitmapintroChoix1.scaleX = 0.6667;
        this.bitmapintroChoix1.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix1);
        this.introVideoChoix1.play();
        setTimeout(this.choix1Loop.bind(this), 4500);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix1);
        }, 4600 );

        /* creation video 1 */
        this.videoChoix1 = document.createElement('video');
        this.videoChoix1.src = 'ressources/video/choix1.webm';

        /*buffer pour loop seamless*/
        this.buffer1 = new createjs.VideoBuffer(this.videoChoix1);
        this.videoChoix1.loop = true;

        this.bitmapChoix1 = new createjs.Bitmap(this.buffer1);
        this.bitmapChoix1.scaleX = 0.6667;
        this.bitmapChoix1.scaleY = 0.6667;
        this.bitmapChoix1.alpha = 0;
        this.stage.addChild(this.bitmapChoix1);

        this.pointage++;

        setTimeout( ()=> {
            //Audio
            this.narrChoix1 = createjs.Sound.play("narr_choix_1");
        }, 4000 );

        //Appel de l'étape 2
        this.jeuEtape2();
    }

    choix1Loop(){

        this.bitmapChoix1.alpha = 1;
        this.videoChoix1.play();
    }

    //Voiture
    faireChoix2(choix) {

        /* creation video intro 2 */
        this.introVideoChoix2 = document.createElement('video');
        this.introVideoChoix2.src = 'ressources/video/intro-choix2.webm';

        this.bufferIntro2 = new createjs.VideoBuffer(this.introVideoChoix2);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix2 = new createjs.Bitmap(this.bufferIntro2);
        this.bitmapintroChoix2.scaleX = 0.6667;
        this.bitmapintroChoix2.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix2);
        this.introVideoChoix2.play();
        setTimeout(this.choix2Loop.bind(this), 4500);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix2);
        }, 4600 );

        /* creation video 2 */
        this.videoChoix2 = document.createElement('video');
        this.videoChoix2.src = 'ressources/video/choix2.webm';

        /*buffer pour loop seamless*/
        this.buffer2 = new createjs.VideoBuffer(this.videoChoix2);
        this.videoChoix2.loop = true;

        this.bitmapChoix2 = new createjs.Bitmap(this.buffer2);
        this.bitmapChoix2.scaleX = 0.6667;
        this.bitmapChoix2.scaleY = 0.6667;
        this.bitmapChoix2.alpha = 0;
        this.stage.addChild(this.bitmapChoix2);

        //Appel de l'étape 2
        this.jeuEtape2();
    }

    choix2Loop(){

        this.bitmapChoix2.alpha = 1;
        this.videoChoix2.play();

        //Audio
        this.narrChoix2 = createjs.Sound.play("narr_choix_2");
    }

    jeuEtape2() {
        //AMBIANCE DE L'ÉTAPE 2
        this.loop2 = createjs.Sound.play("loop_2", {volume: .3, loop: -1});
        createjs.Tween
            .get(this.loop2)
            .to({volume: 1}, 500)

        this.fxEtape2 = createjs.Sound.play("fx_etape_2", {volume: .5});
    }

    //Agriculture vegetale
    faireChoix3(choix) {

        /* creation video intro 3 */
        this.introVideoChoix3 = document.createElement('video');
        this.introVideoChoix3.src = 'ressources/video/intro-choix3.webm';

        this.bufferIntro3 = new createjs.VideoBuffer(this.introVideoChoix3);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix3 = new createjs.Bitmap(this.bufferIntro3);
        this.bitmapintroChoix3.scaleX = 0.6667;
        this.bitmapintroChoix3.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix3);
        this.introVideoChoix3.play();
        setTimeout(this.choix3Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix3);
        }, 6100 );

        /* creation video 3 */
        this.videoChoix3 = document.createElement('video');
        this.videoChoix3.src = 'ressources/video/choix3.webm';

        /*buffer pour loop seamless*/
        this.buffer3 = new createjs.VideoBuffer(this.videoChoix3);
        this.videoChoix3.loop = true;

        this.bitmapChoix3 = new createjs.Bitmap(this.buffer3);
        this.bitmapChoix3.scaleX = 0.6667;
        this.bitmapChoix3.scaleY = 0.6667;
        this.bitmapChoix3.alpha = 0;
        this.stage.addChild(this.bitmapChoix3);

        this.pointage++;

        setTimeout( ()=> {
            //Audio
            this.narrChoix3 = createjs.Sound.play("narr_choix_3");
        }, 1000 );

        //Appel de l'étape 3
        this.jeuEtape3();
    }

    choix3Loop(){
        this.bitmapChoix3.alpha = 1;
        this.videoChoix3.play();
    }

    //Agriculture animale
    faireChoix4(choix) {

        /* creation video intro 4 */
        this.introVideoChoix4 = document.createElement('video');
        this.introVideoChoix4.src = 'ressources/video/intro-choix4.webm';

        this.bufferIntro4 = new createjs.VideoBuffer(this.introVideoChoix4);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix4 = new createjs.Bitmap(this.bufferIntro4);
        this.bitmapintroChoix4.scaleX = 0.6667;
        this.bitmapintroChoix4.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix4);
        this.introVideoChoix4.play();
        setTimeout(this.choix4Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix4);
        }, 6100 );

        /* creation video 4 */
        this.videoChoix4 = document.createElement('video');
        this.videoChoix4.src = 'ressources/video/choix4.webm';

        /*buffer pour loop seamless*/
        this.buffer4 = new createjs.VideoBuffer(this.videoChoix4);
        this.videoChoix4.loop = true;

        this.bitmapChoix4 = new createjs.Bitmap(this.buffer4);
        this.bitmapChoix4.scaleX = 0.6667;
        this.bitmapChoix4.scaleY = 0.6667;
        this.bitmapChoix4.alpha = 0;
        this.stage.addChild(this.bitmapChoix4);

        setTimeout( ()=> {
            //Audio
            this.narrChoix4 = createjs.Sound.play("narr_choix_4");
        }, 2000 );

        //Appel de l'étape 3
        this.jeuEtape3();
    }

    choix4Loop(){

        this.bitmapChoix4.alpha = 1;
        this.videoChoix4.play();

    }

    jeuEtape3() {
        //AMBIANCE DE L'ÉTAPE 3

        this.loop3 = createjs.Sound.play("loop_3", {volume: .3, loop: -1});
        createjs.Tween
            .get(this.loop3)
            .to({volume: 1}, 200)

        this.fxEtape3 = createjs.Sound.play("fx_etape_3", {volume: .5});
    }

    //Magasin local
    faireChoix5(choix) {

        /* creation video intro 5 */
        this.introVideoChoix5 = document.createElement('video');
        this.introVideoChoix5.src = 'ressources/video/intro-choix5.webm';

        this.bufferIntro5 = new createjs.VideoBuffer(this.introVideoChoix5);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix5 = new createjs.Bitmap(this.bufferIntro5);
        this.bitmapintroChoix5.scaleX = 0.6667;
        this.bitmapintroChoix5.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix5);
        this.introVideoChoix5.play();
        setTimeout(this.choix5Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix5);
        }, 6100 );

        /* creation video 5 */
        this.videoChoix5 = document.createElement('video');
        this.videoChoix5.src = 'ressources/video/choix5.webm';

        /*buffer pour loop seamless*/
        this.buffer5 = new createjs.VideoBuffer(this.videoChoix5);
        this.videoChoix5.loop = true;

        this.bitmapChoix5 = new createjs.Bitmap(this.buffer5);
        this.bitmapChoix5.scaleX = 0.6667;
        this.bitmapChoix5.scaleY = 0.6667;
        this.bitmapChoix5.alpha = 0;
        this.stage.addChild(this.bitmapChoix5);

        this.pointage++;

        setTimeout( ()=> {
            //Audio
            this.narrChoix5 = createjs.Sound.play("narr_choix_5");
        }, 1000 );

        //Appel de l'étape 4
        this.jeuEtape4();
    }

    choix5Loop(){

        this.bitmapChoix5.alpha = 1;
        this.videoChoix5.play();
    }

    //Magasin industriel
    faireChoix6(choix) {

        /* creation video intro 6 */
        this.introVideoChoix6 = document.createElement('video');
        this.introVideoChoix6.src = 'ressources/video/intro-choix6.webm';

        this.bufferIntro6 = new createjs.VideoBuffer(this.introVideoChoix6);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix6 = new createjs.Bitmap(this.bufferIntro6);
        this.bitmapintroChoix6.scaleX = 0.6667;
        this.bitmapintroChoix6.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix6);
        this.introVideoChoix6.play();
        setTimeout(this.choix6Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix6);
        }, 6100 );

        /* creation video 6 */
        this.videoChoix6 = document.createElement('video');
        this.videoChoix6.src = 'ressources/video/choix6.webm';

        /*buffer pour loop seamless*/
        this.buffer6 = new createjs.VideoBuffer(this.videoChoix6);
        this.videoChoix6.loop = true;

        this.bitmapChoix6 = new createjs.Bitmap(this.buffer6);
        this.bitmapChoix6.scaleX = 0.6667;
        this.bitmapChoix6.scaleY = 0.6667;
        this.bitmapChoix6.alpha = 0;
        this.stage.addChild(this.bitmapChoix6);

        setTimeout( ()=> {
            //Audio
            this.narrChoix6 = createjs.Sound.play("narr_choix_6");
        }, 1000 );

        //Appel de l'étape 4
        this.avionAnim();
        setInterval(this.avionInterval.bind(this), 20000);
        this.jeuEtape4();
    }

    choix6Loop(){

        this.bitmapChoix6.alpha = 1;
        this.videoChoix6.play();
    }

    jeuEtape4() {
        //AMBIANCE DE L'ÉTAPE 4
        this.loop4 = createjs.Sound.play("loop_4", {volume: .3, loop: -1});
        createjs.Tween
            .get(this.loop4)
            .to({volume: 1}, 200)

        this.fxEtape4 = createjs.Sound.play("fx_etape_4", {volume: .5});
    }

    //Energie renouvelable
    faireChoix7(choix) {

        /* creation video intro 7 */
        this.introVideoChoix7 = document.createElement('video');
        this.introVideoChoix7.src = 'ressources/video/intro-choix7.webm';

        this.bufferIntro7 = new createjs.VideoBuffer(this.introVideoChoix7);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix7 = new createjs.Bitmap(this.bufferIntro7);
        this.bitmapintroChoix7.scaleX = 0.6667;
        this.bitmapintroChoix7.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix7);
        this.introVideoChoix7.play();
        setTimeout(this.choix7Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix7);
        }, 6100 );

        /* creation video 7 */
        this.videoChoix7 = document.createElement('video');
        this.videoChoix7.src = 'ressources/video/choix7.webm';

        /*buffer pour loop seamless*/
        this.buffer7 = new createjs.VideoBuffer(this.videoChoix7);
        this.videoChoix7.loop = true;

        this.bitmapChoix7 = new createjs.Bitmap(this.buffer7);
        this.bitmapChoix7.scaleX = 0.6667;
        this.bitmapChoix7.scaleY = 0.6667;
        this.bitmapChoix7.alpha = 0;
        this.stage.addChild(this.bitmapChoix7);

        this.pointage++;

        setTimeout( ()=> {
            //Audio
            this.narrChoix7 = createjs.Sound.play("narr_choix_7");
        }, 1000 );

        //Appel de l'étape 5
        this.jeuEtape5();
    }

    choix7Loop(){

        this.bitmapChoix7.alpha = 1;
        this.videoChoix7.play();
    }

    //Energie fossile
    faireChoix8(choix) {

        /* creation video intro 8 */
        this.introVideoChoix8 = document.createElement('video');
        this.introVideoChoix8.src = 'ressources/video/intro-choix8.webm';

        this.bufferIntro8 = new createjs.VideoBuffer(this.introVideoChoix8);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix8 = new createjs.Bitmap(this.bufferIntro8);
        this.bitmapintroChoix8.scaleX = 0.6667;
        this.bitmapintroChoix8.scaleY = 0.6667;
        this.stage.addChild(this.bitmapintroChoix8);
        this.introVideoChoix8.play();
        setTimeout(this.choix8Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix8);
        }, 6100 );

        /* creation video 8 */
        this.videoChoix8 = document.createElement('video');
        this.videoChoix8.src = 'ressources/video/choix8.webm';

        /*buffer pour loop seamless*/
        this.buffer8 = new createjs.VideoBuffer(this.videoChoix8);
        this.videoChoix8.loop = true;

        this.bitmapChoix8 = new createjs.Bitmap(this.buffer8);
        this.bitmapChoix8.scaleX = 0.6667;
        this.bitmapChoix8.scaleY = 0.6667;
        this.bitmapChoix8.alpha = 0;
        this.stage.addChild(this.bitmapChoix8);

        setTimeout( ()=> {
            //Audio
            this.narrChoix8 = createjs.Sound.play("narr_choix_8");
        }, 1000 );

        //Appel de l'étape 5
        this.jeuEtape5();
    }

    choix8Loop(){

        this.bitmapChoix8.alpha = 1;
        this.videoChoix8.play();
    }

    jeuEtape5() {
        this.fxEtape5 = createjs.Sound.play("fx_etape_5", {volume: .5});

        this.fuseeAnim();
        setInterval(this.fuseeInterval.bind(this), 14000);
    }

    //Parc naturel
    faireChoix9(choix) {

        /* creation video intro 9 */
        this.introVideoChoix9 = document.createElement('video');
        this.introVideoChoix9.src = 'ressources/video/intro-choix9.webm';

        this.bufferIntro9 = new createjs.VideoBuffer(this.introVideoChoix9);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix9 = new createjs.Bitmap(this.bufferIntro9);
        this.bitmapintroChoix9.scaleX = 0.6667;
        this.bitmapintroChoix9.scaleY = 0.6667;
        this.bitmapintroChoix9.x = -60;
        this.bitmapintroChoix9.y = -45;
        this.stage.addChildAt(this.bitmapintroChoix9, 3);
        this.introVideoChoix9.play();
        setTimeout(this.choix9Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix9);
        }, 6100 );

        /* creation video 9 */
        this.videoChoix9 = document.createElement('video');
        this.videoChoix9.src = 'ressources/video/choix9.webm';

        /*buffer pour loop seamless*/
        this.buffer9 = new createjs.VideoBuffer(this.videoChoix9);
        this.videoChoix9.loop = true;

        this.bitmapChoix9 = new createjs.Bitmap(this.buffer9);
        this.bitmapChoix9.scaleX = 0.6667;
        this.bitmapChoix9.scaleY = 0.6667;
        this.bitmapChoix9.x = -60;
        this.bitmapChoix9.y = -45;
        this.bitmapChoix9.alpha = 0;
        this.stage.addChildAt(this.bitmapChoix9, 3);

        this.pointage++;

        //Audio
        this.fxEtapeFinale = createjs.Sound.play("fx_etape_2", {volume: .5});
        setTimeout( ()=> {
            //Audio
            this.narrChoix9 = createjs.Sound.play("narr_choix_9");
        }, 1000 );

        setTimeout(this.etapeResultat.bind(this), 9000);
    }

    choix9Loop(){

        this.bitmapChoix9.alpha = 1;
        this.videoChoix9.play();
    }

    //Parc de jeu
    faireChoix10(choix) {

        /* creation video intro 10 */
        this.introVideoChoix10 = document.createElement('video');
        this.introVideoChoix10.src = 'ressources/video/intro-choix10.webm';

        this.bufferIntro10 = new createjs.VideoBuffer(this.introVideoChoix10);

        /*création bitmap (createJs)*/
        this.bitmapintroChoix10 = new createjs.Bitmap(this.bufferIntro10);
        this.bitmapintroChoix10.scaleX = 0.6667;
        this.bitmapintroChoix10.scaleY = 0.6667;
        this.bitmapintroChoix10.x = -45;
        this.bitmapintroChoix10.y = -15;
        this.stage.addChildAt(this.bitmapintroChoix10, 3);
        this.introVideoChoix10.play();
        setTimeout(this.choix10Loop.bind(this), 6000);
        setTimeout( ()=> {
            this.stage.removeChild(this.bitmapintroChoix10);
        }, 6100 );

        /* creation video 10 */
        this.videoChoix10 = document.createElement('video');
        this.videoChoix10.src = 'ressources/video/choix10.webm';

        /*buffer pour loop seamless*/
        this.buffer10 = new createjs.VideoBuffer(this.videoChoix10);
        this.videoChoix10.loop = true;

        this.bitmapChoix10 = new createjs.Bitmap(this.buffer10);
        this.bitmapChoix10.scaleX = 0.6667;
        this.bitmapChoix10.scaleY = 0.6667;
        this.bitmapChoix10.x = -45;
        this.bitmapChoix10.y = -15;
        this.bitmapChoix10.alpha = 0;
        this.stage.addChildAt(this.bitmapChoix10, 3);

        //Audio
        this.fxEtapeFinale = createjs.Sound.play("fx_etape_2", {volume: .5});
        setTimeout( ()=> {
            //Audio
            this.narrChoix10 = createjs.Sound.play("narr_choix_10");
        }, 1000 );

        setTimeout(this.etapeResultat.bind(this), 9000);
    }

    choix10Loop(){

        this.bitmapChoix10.alpha = 1;
        this.videoChoix10.play();
    }

    etapeResultat() {

        this.fxEtapeResultat = createjs.Sound.play("fx_good_end", {volume: .5});
        //AMBIANCE
        this.loop5 = createjs.Sound.play("loop_5", {volume: .2, loop: -1});
        createjs.Tween
            .get(this.loop5)
            .to({volume: 1}, 1000)

        if (this.pointage >= 3) {
            this.jeuGoodEnding();
        } else {
            this.jeuBadEnding();
        }
        this.scoreFinale();

    }

    scoreFinale() {
        console.log('function scoreFinale')

        this.texteEnding = new createjs.Bitmap(this.chargeur.getResult('endingTexte'));
        this.texteEnding.scaleX = 0.667;
        this.texteEnding.scaleY = 0.667;
        this.texteEnding.x= this.canvas.width / 2 - this.texteEnding.getBounds().width/2 + 150;
        this.texteEnding.y = this.canvas.height / 2 - 200;
        this.texteEnding.alpha = 0;

        this.pointage = new createjs.Text((this.pointage / 5) * 100 + '%', '150px Arial Black', "#ffffff");
        // this.pointage.x = this.canvas.width / 2 - 150 ;
        this.pointage.x = this.canvas.width / 2 - this.pointage.getBounds().width/2;
        this.pointage.y = this.canvas.height / 2 - 50;
        this.pointage.cache(0, -25, this.pointage.getBounds().width, 200);
        this.pointage.alpha = 0;

        this.recommencer = new createjs.Text('Recommencer', '30px Arial Black', '#ffffff');
        this.recommencer.x= this.canvas.width / 2 - 110;
        this.recommencer.y = this.canvas.height / 2 + 175;
        this.recommencer.alpha = 0;

        this.stage.addChild(this.texteEnding, this.pointage, this.recommencer);

        this.recommencer.addEventListener('click', this.refreshJeu);
        console.log(this.texteEnding.x, this.texteEnding.y, this.pointage.x, this.pointage.y, this.recommencer.x, this.recommencer.y )

        setTimeout(this.tweenAlphaFinal.bind(this), 5000);
        // this.tweenAlphaFinal();
    }

    refreshJeu(){
        location.reload();
    }

    tweenAlphaFinal(){

        createjs.Tween
            .get(this.pointage)
            .to({alpha: 1}, 5000)

        createjs.Tween
            .get(this.texteEnding)
            .to({alpha: 1}, 5000);

        createjs.Tween
            .get(this.recommencer)
            .to({alpha: 1}, 5000);
    }

    jeuGoodEnding() {
        //CREATION VIDEO ET DEPLOIEMENT SUR STAGE;
        let oiseaux = this.chargeur.getResult('oiseaux');
        oiseaux.loop = false;

        let bitmapOiseaux = new createjs.Bitmap(oiseaux);
        this.stage.addChild(bitmapOiseaux);
        oiseaux.play();

        bitmapOiseaux.scaleX = 0.6667;
        bitmapOiseaux.scaleY = 0.6667;

        this.godrays = new createjs.Bitmap(this.chargeur.getResult('godrays'));
        this.godrays.alpha = 0;
        this.stage.addChild(this.godrays);
        createjs.Tween
            .get(this.godrays)
            .to({alpha: .2}, 5000);

        //AMBIANCE DE LA BONNE FIN
        this.goodEnding = createjs.Sound.play("good_end", {volume: 1, loop: 0});
        this.narrGoodEnd = createjs.Sound.play("narr_end_good");
        this.loop1.stop();
        this.loop2.stop();
        this.loop3.stop();
        this.loop4.stop();
        this.loop5.stop();

        //Animation de la couleur du background
        this.canvas.style.backgroundColor = "#B4DBFF";
    }

    jeuBadEnding() {
        //AMBIANCE DE LA MAUVAISE FIN
        this.badEnding = createjs.Sound.play("bad_end", {volume: 1, loop: 0});
        this.narrBadEnd = createjs.Sound.play("narr_end_bad");
        this.loop1.stop();
        this.loop2.stop();
        this.loop3.stop();
        this.loop4.stop();
        this.loop5.stop();

        //Couleur d'overlay
        this.overlayGray = new createjs.Shape();
        this.overlayGray.graphics.beginFill("#A9A9A9").drawRect(0, 0, 1280, 720);
        this.overlayGray.alpha = 0;
        this.stage.addChild(this.overlayGray);
        createjs.Tween
            .get(this.overlayGray)
            .to({alpha: 0.4}, 5000)
    }


}
