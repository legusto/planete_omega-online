export class Interface {

    constructor() {

        let boutonDemarre = document.querySelector('#btnDebuter');

        boutonDemarre.addEventListener('click', () => {
            this.connection();
        })

        this.sectionEtape1 = document.getElementById("sectionEtape1");
        this.sectionEtape2 = document.getElementById("sectionEtape2");
        this.sectionEtape3 = document.getElementById("sectionEtape3");
        this.sectionEtape4 = document.getElementById("sectionEtape4");
        this.sectionEtape5 = document.getElementById("sectionEtape5");

    }

    connection() {
        // Instantiation du client Socket.IO pour communiquer avec le serveur
        this.socket = io("https://schatel.dectim.ca:3009", {
            query: {type: "interface", id: document.getElementById('code').value}
        });
        this.socket.emit("btnIntro", "bouton de l'intro");
        this.fermerEcranTitre();
        this.selectionChoix();
    }




    /* SELECTION DES BOUTONS SUR L'INTERFACE MOBILE, AJOUT D'EVENT TOUCHEND ET FONCTION QUI ENVOIE LES INFO AU SOCKET POUR DIRE LEQUEL FUT CHOISI. setTimeout ENTRE CHAQUE CHOIX POUR PERMETTRE L'ANIMATION*/
    selectionChoix(e) {

        let bouton1 = document.querySelector('#choix1');
        let bouton2 = document.querySelector('#choix2');
        let bouton3 = document.querySelector('#choix3');
        let bouton4 = document.querySelector('#choix4');
        let bouton5 = document.querySelector('#choix5');
        let bouton6 = document.querySelector('#choix6');
        let bouton7 = document.querySelector('#choix7');
        let bouton8 = document.querySelector('#choix8');
        let bouton9 = document.querySelector('#choix9');
        let bouton10 = document.querySelector('#choix10');


        /* ---------- Event Click ---------- */

        /* ÉTAPE 1*/
        bouton1.addEventListener('click', () => {
            this.socket.emit("choix1", "choix 1");
            console.log('bouton1');
            // document.getElementById("sectionEtape1").style.display = "none";
            this.sectionEtape1.style.opacity = "0";
            this.sectionEtape1.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape2, 14000);
        })
        bouton2.addEventListener('click', () => {
            this.socket.emit("choix2", "choix 2");
            console.log('bouton2');
            this.sectionEtape1.style.opacity = "0";
            this.sectionEtape1.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape2, 14000);
        })

        /* ---- ÉTAPE 2*/
        bouton3.addEventListener('click', () => {
            this.socket.emit("choix3", "choix 3");
            console.log('bouton3');
            this.sectionEtape2.style.opacity = "0";
            this.sectionEtape2.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape3, 7000);
        })
        bouton4.addEventListener('click', () => {
            this.socket.emit("choix4", "choix 4");
            console.log('bouton4');
            this.sectionEtape2.style.opacity = "0";
            this.sectionEtape2.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape3, 12000);
        })

        /* ÉTAPE 3*/
        bouton5.addEventListener('click', () => {
            this.socket.emit("choix5", "choix 5");
            console.log('bouton5');
            this.sectionEtape3.style.opacity = "0";
            this.sectionEtape3.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape4, 8000);
        })
        bouton6.addEventListener('click', () => {
            this.socket.emit("choix6", "choix 6");
            console.log('bouton6');
            this.sectionEtape3.style.opacity = "0";
            this.sectionEtape3.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape4, 14000);
        })

        /* ÉTAPE 4*/
        bouton7.addEventListener('click', () => {
            this.socket.emit("choix7", "choix 7");
            console.log('bouton7');
            this.sectionEtape4.style.opacity = "0";
            this.sectionEtape4.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape5, 8000);
        })
        bouton8.addEventListener('click', () => {
            this.socket.emit("choix8", "choix 8");
            console.log('bouton8');
            this.sectionEtape4.style.opacity = "0";
            this.sectionEtape4.style.pointerEvents = "none";
            setTimeout(this.interfaceEtape5, 10000);
        })

        /* ÉTAPE 5*/
        bouton9.addEventListener('click', () => {
            this.socket.emit("choix9", "choix 9");
            console.log('bouton9');
            this.sectionEtape5.style.opacity = "0";
            this.sectionEtape5.style.pointerEvents = "none";
            setTimeout(this.refresh, 12000);
        })
        bouton10.addEventListener('click', () => {
            this.socket.emit("choix10", "choix 10");
            console.log('bouton10');
            this.sectionEtape5.style.opacity = "0";
            this.sectionEtape5.style.pointerEvents = "none";
            setTimeout(this.refresh, 12000);

        })

    } //Fin selectionChoix

    /* ---------- Changement d'interfaces mobiles avec réponses ---------- */

    interfaceEtape1() {
        console.log('etape1');
        this.sectionEtape1.style.opacity = "1";
        this.sectionEtape1.style.pointerEvents = "auto";
    }

    interfaceEtape2() {
        console.log('etape2');
        this.sectionEtape2.style.opacity = "1";
        this.sectionEtape2.style.pointerEvents = "auto";
    }

    interfaceEtape3() {
        console.log('etape3');
        this.sectionEtape3.style.opacity = "1";
        this.sectionEtape3.style.pointerEvents = "auto";
    }

    interfaceEtape4() {
        console.log('etape4');
        this.sectionEtape4.style.opacity = "1";
        this.sectionEtape4.style.pointerEvents = "auto";
    }

    interfaceEtape5() {
        console.log('etape5');
        this.sectionEtape5.style.opacity = "1";
        this.sectionEtape5.style.pointerEvents = "auto";
    }

    refresh(){
        location.reload();
    }

    fermerEcranTitre(){
        document.getElementById("ecranTitreMobile").style.display = "none";
        setTimeout(this.interfaceEtape1, 26000);
    }




}
