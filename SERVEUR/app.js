// Code de test rapide *****************************************************************************
// let http = require('http');
//
// let compteur = 0;
//
// let serveur = http.createServer((requete, reponse) => {
//   reponse.end("Allo! #" + ++compteur);
// })
//
// serveur.listen(3000, () => {
//   console.log("Serveur fonctionnel sur le port", serveur.address().port);
// })
// *************************************************************************************************

// import {Interface} from "./public_html/interface/scripts/Interface.js";
// import {Jeu} from "./public_html/planete_omega/scripts/Jeu.js";

/* ---------- Config de base (https, path, socket, etc)---------- */


// Importation des modules de Node.js
const fs = require('fs')
const https = require('https');
const path = require('path');
let SocketIO = require('socket.io');


// Configurations
const config = {
    adresse: "schatel.dectim.ca",
    port: 3009,
    certificat: fs.readFileSync(path.resolve() + '/securite/certificat.pem'),
    cle: fs.readFileSync(path.resolve() + '/securite/cle.pem')
}

// Instantiation d'un serveur HTTPS et d'un serveur de WebSocket utlisant le serveur HTTP comme couche de transport
const serveur = https.createServer({key: config.cle, cert: config.certificat});
const io = SocketIO(serveur);

// Démarrage du serveur HTTPS
serveur.listen(config.port, config.adresse, () => {
    console.log("Le serveur est prêt et écoute sur le port " + serveur.address().port);
});


// Liste des connexions de toutes les interfaces (appareils mobiles)
let interfaces = [];

// Liste des connexions de tous les jeux (pages web)
let jeuxTab = [];


// Mise en place d'un écouteur pour traiter les connexions de client et les données envoyées par ceux-ci.

io.on("connection", socket => {

    // On vérifie si la connexion provient d'une interface ou d'un jeu
    if (socket.handshake.query.type === "interface") {
        gererNouvelleInterface(socket);
    } else if (socket.handshake.query.type === "jeu") {
        gererNouveauJeu(socket);
    } else {
        socket.disconnect();
    }

});

function gererNouvelleInterface(socket) {

    console.log("Connexion d'une interface", socket.handshake.query.id);
    // socket.on('choix1', socket.emit('selectionChoix1', message));
    // new Interface();

    /* --------- AJOUT ET EMISSION D'EVENEMENT POUR CHAQUE CHOIX, DE L'INTERFACE VERS LE JEU --------- */

    let jeuTrouver = jeuxTab.find(jeu => jeu.handshake.query.id === socket.handshake.query.id);

    // if (!jeuTrouver) {
    //     socket.emit('mauvaisCode');
    //     socket.disconnect();
    //     return;
    // }

if(jeuTrouver){
    socket.on("choix1", message => {
        jeuTrouver.emit("selectionChoix1", message)
    })
    socket.on("choix2", message => {
        jeuTrouver.emit("selectionChoix2", message)
    })
    socket.on("choix3", message => {
        jeuTrouver.emit("selectionChoix3", message)
    })
    socket.on("choix4", message => {
        jeuTrouver.emit("selectionChoix4", message)
    })
    socket.on("choix5", message => {
        jeuTrouver.emit("selectionChoix5", message)
    })
    socket.on("choix6", message => {
        jeuTrouver.emit("selectionChoix6", message)
    })
    socket.on("choix7", message => {
        jeuTrouver.emit("selectionChoix7", message)
    })
    socket.on("choix8", message => {
        jeuTrouver.emit("selectionChoix8", message)
    })
    socket.on("choix9", message => {
        jeuTrouver.emit("selectionChoix9", message)
    })
    socket.on("choix10", message => {
        jeuTrouver.emit("selectionChoix10", message)
    })
    socket.on("btnIntro", message => {
        jeuTrouver.emit("fermerIntro", message)
    })
    // socket.on('disconnect', () => {
    //     socket.removeAllEventListeners();
    // })
}else if(!jeuTrouver){
    socket.emit('mauvaisCode');
    socket.disconnect();
}

}

function gererNouveauJeu(socket) {

    console.log("Connexion d'une page web de jeu", socket.handshake.query.id);
    // new Jeu();
    jeuxTab.push(socket);

    socket.on('disconnect', () => {
        console.log("Déconnexion d'une page de jeu!");
        jeuxTab = jeuxTab.filter(item => item !== socket)
    });

}

// ws://67.212.82.134:3000/socket.io/?EIO=2&transport=websocket
